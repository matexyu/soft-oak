/**
 * 16x Sg90 Servos Screen control, Hello World. 
 * 
 */

import processing.serial.*;

final int k12bitMinPwmValue = 0;
final int k12bitActivationPwmValue = (int)(4096.0 * 60.0 / 255.0);
final int k12bitMinPwmValueOnceAvtive = (int)(4096.0 * 36.0 / 255.0);
final int k12bitMaxPwmValue = 4095;

boolean[] isErmMotorActive = new boolean[16];

final float kScreenMargin = 0.0; // the larger, the "farther" away in the computer screen

Serial myPort;  // Create object from Serial class

void setup() 
{
  size(1024, 1024, P3D);
  frameRate(60);
  
  // Print a list of the serial ports, for debugging purposes:
  printArray(Serial.list());
  
  String portName = Serial.list()[0];
  myPort = new Serial(this, portName, 115200);
  myPort.clear();
  
  for (int i = 0; i < 16; i++)
  {
    bufferPwm[i] = k12bitMaxPwmValue;
  }
  
  ellipseMode(CENTER);
}

float buffer[] = new float[4 * 4];
int bufferPwm[] = new int[4 * 4];
byte frameBytes[] = new byte[3 * 16];

int clamp(int v, int a, int b)
{
  if(a > b) return clamp(v, b, a);
  return v < a ? a : v > b? b : v;
}

float clamp(float v, float a, float b)
{
  if(a > b) return clamp(v, b, a);
  return v < a ? a : v > b? b : v;
}

void drawErmMotor(float x, float y, float intensity, float max_radius)
{
  final float d = intensity * (max_radius - 10.0) * 2.0 + 10.0;
  final float a = ((intensity * intensity) * 0.6 + 0.4);
  fill(255, 64, 64, a*255.0);
  
  pushMatrix();
  translate(x, y);
  ellipse(0, 0, d, d);
  popMatrix();
}

void drawSimulation()
{
  noStroke();
  
  final float h = height;
  final float w = h;
  
  float sw = 0.5 *  w / (6.0 + kScreenMargin * 2.0);
  for (int y = 0; y < 4; y++)
  {
    float sy = map(y, -1.0 - kScreenMargin, 4.0 + kScreenMargin, 0, h);
    for (int x = 0; x < 4; x++)
    {
      float sx = width * 0.5 + map(x, -1.0 - kScreenMargin, 4.0 + kScreenMargin, -w * 0.5, w * 0.5);
      drawErmMotor(sx, sy, buffer[y*4 + x], sw);
    }
  }
}

float mTimeSeconds = 0.0;

void generateAnimation()
{
  mTimeSeconds += 0.25 * (sin(mTimeSeconds*0.2)*0.25 + 1.0) / 60.0;//final float t = 0.6 * millis() / 1000.0;
  float t = mTimeSeconds;
  for (int y = 0; y < 4; y++)
  {
    for (int x = 0; x < 4; x++)
    {
      int i = x + y * 4;
      float k = sin(t * PI * 2.0 + sin(t*.1*PI) * (PI * i / 8.0)) * 0.5 + 0.5;
      
      int j = y + x * 4;
      buffer[j] = k; 
    }
  }
  
  final float range = k12bitMaxPwmValue - k12bitMinPwmValue;
  for (int i = 0; i < 16; i++)
  {
    int pwm_value = (int)(buffer[i] * range + k12bitMinPwmValue); 
    bufferPwm[i] = pwm_value;
  }
}

void animateFromMouse()
{
  final float h = height;
  final float w = h;
  final float max_dist = sqrt(width*width + height*height);
  
  float intensity = 0.0;
  
  for (int y = 0; y < 4; y++)
  {
    float sy = map(y, -1.0 - kScreenMargin, 4.0 + kScreenMargin, 0, h);
    for (int x = 0; x < 4; x++)
    {
      if(mousePressed)
      {
        float sx = width * 0.5 + map(x, 
                                     -1.0 - kScreenMargin, 4.0 + kScreenMargin, 
                                     -w * 0.5, w * 0.5);
        
        
        
        float dx = mouseX - sx;
        float dy = mouseY - sy;
        float kd = sqrt(dx * dx + dy * dy) / max_dist;
        intensity = 1.0 / (80.0 * kd * kd + 0.1);
        intensity *= intensity;
        intensity = clamp(intensity, 0.0, 1.0);
      }
      
      final int i = x + y * 4;
      float k = (intensity < buffer[i] ? 0.015 : 0.15);
      buffer[i] = buffer[i] * (1.0 - k) + intensity * k;
      
      int pwm_value = (int) map(buffer[i], 0.0, 1.0, 0, k12bitMaxPwmValue);
      
      if (pwm_value > k12bitActivationPwmValue)
      {
        isErmMotorActive[i] = true;
      }
      else if (isErmMotorActive[i] == true && 
               pwm_value < k12bitMinPwmValueOnceAvtive)
      {
        isErmMotorActive[i] = false;
      }
      
      if(isErmMotorActive[i])
      {
        bufferPwm[i] = pwm_value;
      }
      else
      {
        bufferPwm[i] = 0;
      }
    }
  }
}

void sendToServoScreen()
{
  for (int i = 0; i < 16; i++)
  {    
    int j = i * 3;
    int pwm_value = bufferPwm[i];
    
    frameBytes[j] = (byte)((i & 0x7f) | 128);
    j++;
    frameBytes[j] = (byte)((pwm_value >> 7) & 0x7f);
    j++;
    frameBytes[j] = (byte)(pwm_value & 0x7f); 
  }

  myPort.write(frameBytes);
}

void draw() 
{
  background(0);
  
  //generateAnimation();
  
  animateFromMouse();
  
  sendToServoScreen();
  
  drawSimulation();
}
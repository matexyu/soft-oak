/*
  16x Servos Sg 90: Serial Port control
  
  Based on:
   . PCA 9685: 16-Channel 12-bit PWM/Servo Driver board, I2C controlled
     . Original Adafruit version: https://www.adafruit.com/product/815
     . PWM/Servo Driver library here: https://github.com/adafruit/Adafruit-PWM-Servo-Driver-Library
       Available via Arduino "Install Libraries..." under "Adafruit PWM Servo Driver Library"

  --------------------
    PCA 9685 NOTES:
  --------------------
  Connect the PCA 9685 SDA and SCL inputs to the proper pins, depending on the used Arduino board.
  For example, on the Arduino Nano:
   . SDA: Analog 4 pin
   . SCL: Analog 5 pin

  Important: the PCA 9685 Output Enable (OE) input of the driver *is active LOW*

  SLIP Serial R/W libraries:
   . Arduino: https://github.com/bakercp/PacketSerial
   . Processing: https://github.com/tambien/oscuino/blob/master/examples/Processing/oscuinoSerial/oscuinoSerial.pde

   Mathieu Bosi 2016
*/

#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

// Called this way, it uses the default address: 0x40
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();
// You can also call it with a different address you want, e.g.
//  Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver(0x41);

const int kRefreshIntervalHz = 400; // Analog servos run at ~50 Hz updates

const int16_t k12bitMinPwmValue = 100;
const int16_t k12bitMaxPwmValue = 4095;

const int16_t kNumServos = 16;

struct ServoData 
{
  ServoData()
  {
    targetPwmValue = k12bitMinPwmValue;
    didTargetPwmValueChange = true;
  }
  int16_t targetPwmValue;
  bool didTargetPwmValueChange;
};

ServoData mServoPwmValue[kNumServos];

void setup()
{ 
  Serial.begin(115200);
  
  pwm.begin();
  pwm.setPWMFreq(kRefreshIntervalHz);
  
  // must be changed after calling Wire.begin() (inside pwm.begin())
  TWBR = 12; // upgrade to 400KHz!
}

bool mRefreshNeeded = true;

void loop()
{ 
  int16_t servo_id = 0;
  
  while (servo_id != 15)
  {  
    byte b;
    // Seek for sync byte
    do {
      Serial.readBytes(&b, 1);
    } while((b & 128) == 0);
    
    b &= 0x7f; // mask-out the constol bit
    servo_id = b;
    
    byte buf[2];
    Serial.readBytes(buf, 2);
    const int16_t msb = buf[0];
    const int16_t lsb = buf[1];
    const int16_t target_pwm_value = (msb << 7) | lsb;

    if (mServoPwmValue[servo_id].targetPwmValue != target_pwm_value)
    {
      mServoPwmValue[servo_id].targetPwmValue = target_pwm_value;
      mServoPwmValue[servo_id].didTargetPwmValueChange = true;
      mRefreshNeeded = true;
    }
  }
  
  // Blam! Feed the PWM controller chip as fast as possible
  if (mRefreshNeeded)
  {    
    for (int16_t i = 0; i < kNumServos; i++)
    {
      if (mServoPwmValue[i].didTargetPwmValueChange)
      {
        pwm.setPWM(i, 0, mServoPwmValue[i].targetPwmValue);
        mServoPwmValue[i].didTargetPwmValueChange = false;
      }
    }
    mRefreshNeeded = false;
  }
  
}



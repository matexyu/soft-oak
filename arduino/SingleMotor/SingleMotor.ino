/*
  Test code for controlling a single Eccentric Rotating Mass motor
  connected to Arduino PWM pin 3.
  The intensity of the rotation/vibration is controlled via a potentiometer
  connected to Analog pin 0.

  Use the circuit at: http://learningaboutelectronics.com/Articles/Vibration-motor-circuit.php

  NOTES:
  Motor Activation and deactivation PWM values discovered manually by 
  looking at the PWM values sent over Serial.
  Motor supply 5V over an extra USB supply (NOT via pins!).

  Mathieu Bosi 2016
*/

const int kMotorPin = 3;
const int kPotentiometerAnalogPin = 0;

// Minimum PWM value needed to start the motor
const int activation_pwm_value = 60;
// When the motor is already spinning we can then reach lower rotation speeds.
const int min_pwm_value_once_active = 36; 
// Schmidt trigger flag
bool is_activation_pwm_value_crossed = false;

// debounce repeating computed PWM values
int pwm_val_old = -1;

void setup()
{
  pinMode(kMotorPin, OUTPUT);
  Serial.begin(115200);
}

void loop()
{
  const int val = analogRead(kPotentiometerAnalogPin) >> 2;
  int pwm_val = 0;
  
  if (is_activation_pwm_value_crossed == false)
  {
    if (val >= activation_pwm_value)
    {
      is_activation_pwm_value_crossed = true;
      pwm_val = val;
    }
    else 
    {
      pwm_val = 0;
    }
  }
  else // is_activation_value_crossed == true
  {
    if (val < min_pwm_value_once_active)
    {
      pwm_val = 0;
      is_activation_pwm_value_crossed = false;
    }
    else
    {
      pwm_val = val;
    }
  }
  
  if (pwm_val != pwm_val_old)
  {
    analogWrite(kMotorPin, pwm_val);
    Serial.println(pwm_val, DEC);
    pwm_val_old = pwm_val;
  }
}

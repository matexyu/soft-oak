/*
  Haptic Set
  
  Based on:
   . PCA 9685: 16-Channel 12-bit PWM/Servo Driver board, I2C controlled
     . Original Adafruit version: https://www.adafruit.com/product/815
     . PWM/Servo Driver library here: https://github.com/adafruit/Adafruit-PWM-Servo-Driver-Library
       Available via Arduino "Install Libraries..." under "Adafruit PWM Servo Driver Library"
   . Drive circuitry for 16+ Eccentric Rotating Mazz mini-motors, driven by as many 2N2222 + 1K resistor + caps

  --------------------
    PCA 9685 NOTES:
  --------------------
  Connect the PCA 9685 SDA and SCL inputs to the proper pins, depending on the used Arduino board.
  For example, on the Arduino Nano:
   . SDA: Analog 4 pin
   . SCL: Analog 5 pin

  Important: the PCA 9685 Output Enable (OE) input of the driver *is active LOW*

   Mathieu Bosi 2016
*/

#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

// Called this way, it uses the default address: 0x40
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();
// You can also call it with a different address you want, e.g.
//  Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver(0x41);

uint8_t twbrbackup;

void setup() 
{
  pwm.begin();
  pwm.setPWMFreq(1600);  // 1600 is the maximum PWM frequency

  // save I2C bitrate
  twbrbackup = TWBR;
  
  // must be changed after calling Wire.begin() (inside pwm.begin())
  TWBR = 12; // upgrade to 400KHz!
}

void loop() 
{
  cycleOutputs();
  //doPwmWave();
}

void cycleOutputs()
{
  // Cycle through all outputs one by one
  const uint16_t on_value = 4095;
  
  for (uint8_t active_channel_id = 0; active_channel_id < 16; active_channel_id++)
  {
    for (uint8_t i = 0; i < 16; i++)
    {
      const uint16_t pwm_value = (i == active_channel_id ? on_value : 0);
      pwm.setPWM(i, 0, pwm_value);
    }
    int32_t val = analogRead(0);
    val *= val;
    val >>= 10;
    delay(10 + val);
  }
}

void doPwmWave()
{
  // Drive each PWM in a 'wave'
  for (uint16_t i = 0; i < 4096; i ++) 
  {
    for (uint8_t channel_id = 0; channel_id < 16; channel_id++) 
    {
      pwm.setPWM(channel_id, 0, (i + (4096 / 16) * channel_id) % 4096);
    }
  }
}


